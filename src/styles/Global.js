import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
*{
    margin: 0;
    padding: 0;
    box-sizing: border-boxing;
    outline: 0;
}

:root {
    --white:#F5F5F5 ;
    --vanilla:#F1EDE0;
    --black: #0c0d0d;
    --orange: #D27A45;
    --gray: #666360;
    --red: #c53030;
}

body {
    background: var(--vanilla);
    color: var(--black);
}

body, input, button {
    font-family: "PT serif", serif;
    font-size: 1rem
}

h1,h2,h3,h4{
    font-family: "Roboto Mono", monospace;
    font-weight: 700;

}

button {
    cursor: pointer
}

a {
    text-decoration: none
}

`;