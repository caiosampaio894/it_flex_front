import React, { useEffect, useState } from "react";
import { Container, InputContainer, CertificateContainer } from "./styles";
import Input from "../components/Input";
import { useForm } from "react-hook-form";
import Button from "../components/Button";
import Card from "../components/Card";
import api from "../service/api";
import { toast } from "react-toastify";


const Home = () => {
  const { register, handleSubmit } = useForm();
  const [certificates, setCertificate] = useState([]);

  const loadCertificate = () => {
    api
      .get("/certificate")
      .then((response) => {
        const apiCertificate = response.data.map((certificate) => ({
          ...certificate,
          created_at: new Date(certificate.created_at).toLocaleDateString("pt-BR", {
            day: "2-digit",
            month: "long",
            year: "numeric",
          }),
        }));
        setCertificate(apiCertificate);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    loadCertificate();
  }, []);

  const onSubmit = ({ username, name, description, groups, expiration }) => {
    const addCertificates = {username, name, description, groups, expiration}
    console.log(addCertificates)
    if (!username) {
      return toast.error("Complete o campo para enviar uma tarefa ");
    }
    api
      .post(
        "/certificate", addCertificates
      )
      .then((response) => loadCertificate());
  };

  const handleCompleted = (id) => {
    const newCertificate = certificates.filter((certificate) => certificate.id !== id);

    api
      .patch(
        `/certificate/${id}`
      )
      .then((response) => setCertificate(newCertificate));
  };

  return (
    <Container>
      <InputContainer onSubmit={handleSubmit(onSubmit)}>
        <time>18 de outubro de 2021</time>
        <section>
          <Input
            placeholder="Novo Certificado"
            register={register}
            name="username"
            error=""
          />
          <Input
            placeholder="nome"
            register={register}
            name="name"
            error=""
          />
          <Input
            placeholder="descrição"
            register={register}
            name="description"
            error=""
          />
          <Input
            placeholder="grupos"
            register={register}
            name="groups"
            error=""
          />
          <Input
            placeholder="validade em dias"
            register={register}
            name="expiration"
            error=""
          />
          <Button type="submit">Adicionar</Button>
        </section>
      </InputContainer>
      <CertificateContainer>
        {certificates.map((certificate) => (
          <Card
            key={certificate.id}
            title={certificate.description}
            date={certificate.created_at}
            onClick={() => handleCompleted(certificate.id)}
          />
        ))}
      </CertificateContainer>
    </Container>
  );
};

export default Home;