import { Route, Switch } from "react-router";
import Home from "../home";


function Routes() {

  return (
    <Switch>
      <Route exact path="/">
        <Home/>
      </Route>
    </Switch>
  );
}

export default Routes;