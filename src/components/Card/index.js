import Button from "../Button";
import { Container } from "./styles";

const Card = ({ title, date, onClick }) => {
  return (
    <Container>
      <span>
           {title}
      </span>
      <hr />
      <time>
         {date}
      </time>
      <Button onClick={onClick}>Concluir</Button>
    </Container>
  );
};

export default Card;
