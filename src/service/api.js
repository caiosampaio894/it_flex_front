import axios from 'axios';

const api = axios.create({
  baseURL: "https://it-flex-caio-2222.herokuapp.com/",
});

export default api;